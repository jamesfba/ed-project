# Setup

You will need to add a new page via the wp backend and assign the homepage template.
Default content is set to display using ACF free. It currently doesnt add the 2 images to the body so these need to be added via the backend.

You will need to scroll past the main content block to find the custom fields.

Once the template is populated with content you will need to go to `Settings >> Reading` and set the homepage page as the homepage.

The oage should be mobile responsive and has a  mobile menu thats powered by WP menus.
