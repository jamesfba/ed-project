{{--
  Template Name: Homepage
--}}

@extends('layouts.app')

  @include('partials.page-header')

  <div class="banner">
    <div class="banner__image"></div>
      <div class="banner__inner">
          <h1>Elementary Test Page</h1>
          <a href="#" class="btn circle-hover">Button</a>
      </div>
  </div>


  <div class="wrapper">
     <div class="row ed-row">
        <div class="col-xs-12
              col-sm-6
              col-md-6
              col-lg-6">
            <div class="box">
              <img src="{{ get_field('image_row_1') }}">
            </div>
        </div>
        <div class="col-xs-12
              col-sm-6
              col-md-6
              col-lg-6">
            <div class="box">
              <h2>{{ get_field('row_1_title') }}</h2>
                <p>{{ get_field('row_1_description') }}</p>
            </div>
        </div>
      </div>



      <div class="row ed-row">
        
        <div class="col-xs-12
              col-sm-6
              col-md-6
              col-lg-6">
            <div class="box">
                <h2>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut.</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eleifend tincidunt nibh eu bibendum. Aenean leo dolor, mattis et lacus id, sagittis aliquam nunc. Duis et risus sit amet sapien varius aliquam nec a lectus. Suspendisse congue molestie elit, a hendrerit lectus sagittis eget. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </div>
        </div>
        <div class="col-xs-12
              col-sm-6
              col-md-6
              col-lg-6">
            <div class="box">
              <img src="<?php echo get_template_directory_uri(); ?>/assets/images/image-right.jpg">
            </div>
        </div>
      </div>

<div class="row ed-row">
        
        <div class="col-xs-12
              col-sm-offset-2 col-sm-8 
              col-md-offset-3 col-md-6 
              col-lg-offset-3 col-lg-6 ">
            <div class="box">
                <form>
                  <div class="form-element">
                    <label for="name">Name</label> 
                    <input name="name" type="text" /> 
                  </div>
                  <div class="form-element">
                    <label for="email">Email</label>
                    <input name="email" type="email" /> 
                  </div>
                  <div class="form-element">
                    <label for="phone">Telephone</label>
                    <input name="phone" type="text" /> 
                  </div>
                  <div class="form-element">
                    <label for="message">Message</label>
                    <textarea name="message"></textarea> 
                  </div>
                  <div class="form-element centre">
                    <button type="submit" value="send" class="btn dark circle-hover">Send</button>
                  </div>
                </form>
            </div>
        </div>


  </div>
</div>



