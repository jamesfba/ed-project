<div class="main-container">
  <header class="main-header">
    <a href="{{ home_url('/') }}" class="logo">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png" alt="{{ get_bloginfo('name', 'display') }}" />
    </a>

    <nav>
    	@if (has_nav_menu('primary_navigation'))
        {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
      @endif
        <!-- <a href="#">Nav Link</a>
        <a href="#">Nav Link</a>
        <a href="#">Nav Link</a>
        <a href="#" class="outline">Button</a> -->
    </nav>
    <button class="menu-toggle">
      <span></span>
      <span></span>
      <span></span>
    </button>
  </header>