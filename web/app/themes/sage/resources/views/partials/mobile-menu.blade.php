<div class="mobile-menu">

<nav>
    	@if (has_nav_menu('primary_navigation'))
        {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
      @endif
        <!-- <a href="#">Nav Link</a>
        <a href="#">Nav Link</a>
        <a href="#">Nav Link</a>
        <a href="#" class="outline">Button</a> -->
    </nav>
</div>